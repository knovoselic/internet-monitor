json.result_labels [
  'Server ID',
  '5th percentile',
  'Average download',
  'Maximum download'
]
json.server_metadata @server_metadata
json.result @result
