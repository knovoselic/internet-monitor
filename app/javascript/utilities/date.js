function subtractMonths(date, months) {
  const initial_month = date.getMonth();
  date.setMonth(initial_month - months);
  while (date.getMonth() === initial_month) {
    date.setDate(date.getDate() - 1);
  }

  return date;
}

export { subtractMonths };
