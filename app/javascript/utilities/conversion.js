export function bitsToMbits(x, precision = 0) {
  const BITS_IN_MEGABIT = 1024 * 1024;
  const DECIMAL_PLACES = 10 ** precision;

  return Math.round(
    (x / BITS_IN_MEGABIT + Number.EPSILON) * DECIMAL_PLACES
  ) / DECIMAL_PLACES;
}
