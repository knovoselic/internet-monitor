import Dygraph from 'dygraphs';
import { subtractMonths } from '../utilities/date';
import { bitsToMbits } from '../utilities/conversion';
import { darkenColor, DYGRAPH_COLORS } from '../utilities/colors';

// Multiple column bar chart
function multiColumnBarPlotter(e) {
  // We need to handle all the series simultaneously.
  if (e.seriesIndex !== 0) return;

  var g = e.dygraph;
  var ctx = e.drawingContext;
  var sets = e.allSeriesPoints;
  var y_bottom = e.dygraph.toDomYCoord(0);

  // Find the minimum separation between x-values.
  // This determines the bar width.
  var min_sep = Infinity;
  for (var j = 0; j < sets.length; j++) {
    var points = sets[j];
    for (var i = 1; i < points.length; i++) {
      var sep = points[i].canvasx - points[i - 1].canvasx;
      if (sep < min_sep) min_sep = sep;
    }
  }
  var bar_width = Math.floor(2.0 / 3 * min_sep);

  var fillColors = [];
  var strokeColors = g.getColors();
  for (var i = 0; i < strokeColors.length; i++) {
    fillColors.push(darkenColor(strokeColors[i]));
  }

  for (var j = 0; j < sets.length; j++) {
    ctx.fillStyle = fillColors[j];
    ctx.strokeStyle = strokeColors[j];
    for (var i = 0; i < sets[j].length; i++) {
      var p = sets[j][i];
      var center_x = p.canvasx;
      var x_left = center_x - (bar_width / 2) * (1 - j / (sets.length - 1));

      ctx.fillRect(x_left, p.canvasy,
        bar_width / sets.length, y_bottom - p.canvasy);

      ctx.strokeRect(x_left, p.canvasy,
        bar_width / sets.length, y_bottom - p.canvasy);
    }
  }
}

$(() => {
  console.log('here');
  $('.measurement_server_performance_chart').each((index, element) => {
    const isp = element.getAttribute('data-isp');

    $.ajax({
      url: '/api/v1/chart/measurement_server_performance.json',
      data: { isp },
      success: (response) => {
        new Dygraph(
          element,
          response.result, {
            colors: DYGRAPH_COLORS,
            labels: response.result_labels,
            legend: 'always',
            plotter: multiColumnBarPlotter,
            animatedZooms: true,
            dateWindow: [-1, response.result.length],
            axes: {
              x: {
                axisLabelWidth: 60,
                axisLabelFormatter: (index) => {
                  const metadata = response.server_metadata[index];

                  if (metadata) return metadata.name;
                },
                valueFormatter: (index) => {
                  const metadata = response.server_metadata[index];

                  if (metadata) {
                    return `[${metadata.server_id}] ${metadata.name} (${metadata.measurements}) <br/>`;
                  }
                }
              },
              y: {
                axisLabelWidth: 60,
                axisLabelFormatter: (y) => {
                  return bitsToMbits(y) + ' Mbps';
                },
                valueFormatter: (value) => {
                  return `${bitsToMbits(value, 2)} Mbps <br/>`;
                }
              }
            }
          }
        )
      }
    });
  });
});
