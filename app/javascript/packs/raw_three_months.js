import Dygraph from 'dygraphs';
import { subtractMonths } from '../utilities/date';
import { bitsToMbits } from '../utilities/conversion';
import { DYGRAPH_COLORS } from '../utilities/colors';

const end_date = new Date().toISOString().slice(0, 10);
const start_date = subtractMonths(new Date(end_date), 3).toISOString().slice(0, 10);
const query_parameters = new URLSearchParams(window.location.search);
const isp = query_parameters.get('isp');

$.ajax({
  url: '/api/v1/chart/raw_data.json',
  data: { start_date, end_date, isp },
  success: (response) => {
    response.result.map(x => x[0] = new Date(x[0]));
    new Dygraph(
      document.getElementById('raw_three_months'),
      response.result, {
        colors: DYGRAPH_COLORS,
        labels: response.result_labels,
        legend: 'always',
        animatedZooms: true,
        series: { 'Ping': { axis: 'y2' } },
        axes: {
          y: {
            axisLabelWidth: 60,
            axisLabelFormatter: (y) => {
              return bitsToMbits(y) + ' Mbps';
            },
            valueFormatter: (value) => {
              return bitsToMbits(value, 2);
            }
          }
        }
      }
    )
  }
});
