import Dygraph from 'dygraphs';
import { bitsToMbits } from '../utilities/conversion';
import { DYGRAPH_COLORS } from '../utilities/colors';

const query_parameters = new URLSearchParams(window.location.search);
const isp = query_parameters.get('isp');

function minutesToHoursAndMinutesString(value) {
  const hours = String(Math.floor(value / 60)).padStart(2, '0');
  const minutes = String(value % 60).padStart(2, '0');

  return `${hours}:${minutes}`;
}

$.ajax({
  url: '/api/v1/chart/time_of_day.json',
  data: { isp },
  success: (response) => {
    new Dygraph(
      document.getElementById('time_of_day'),
      response.result, {
        colors: DYGRAPH_COLORS,
        labels: response.result_labels,
        legend: 'always',
        animatedZooms: true,
        includeZero: true,
        series: { 'Ping': { axis: 'y2' } },
        axes: {
          x: {
            pixelsPerLabel: 40,
            axisLabelFormatter: minutesToHoursAndMinutesString,
            valueFormatter: minutesToHoursAndMinutesString,
          },
          y: {
            axisLabelWidth: 60,
            axisLabelFormatter: (y) => {
              return bitsToMbits(y) + ' Mbps';
            },
            valueFormatter: (value) => {
              return bitsToMbits(value, 2);
            }
          }
        }
      }
    )
  }
});
