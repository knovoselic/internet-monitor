class Measurement < ApplicationRecord
  has_one :measurement_client, dependent: :destroy
  has_one :measurement_server, dependent: :destroy

  accepts_nested_attributes_for :measurement_client
  accepts_nested_attributes_for :measurement_server

  validates :download, :upload, presence: true

  scope :by_isp, ->(isp) do
    if isp == 'all'
      Measurement
    else
      Measurement.joins(:measurement_client)
                 .where(measurement_clients: {isp: isp})
    end
  end
end
