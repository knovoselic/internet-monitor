class ApiController < ActionController::API
  AUTHORIZATION_HEADER = 'Authorization-Key'

  before_action :authorize_with_api_key
  rescue_from ActiveRecord::RecordInvalid, with: :validation_failed

  private

  def authorize_with_api_key
    expected_key = Rails.application.credentials.api_authorization_key
    return if request.headers[AUTHORIZATION_HEADER] == expected_key

    render json: {error: 'unauthorized'}, status: :unauthorized
  end

  def validation_failed(exception)
    render json: {
      validation_errors: exception.record.errors
    }, status: :unprocessable_entity
  end
end
