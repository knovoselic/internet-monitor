class Api::V1::Chart::RollingAveragesController < ApplicationController
  def index
    @result = rolling_averages
  end

  private

  def end_date
    @end_date ||= Date.iso8601 params.require(:end_date)
  end

  def isp
    params.require :isp
  end

  def rolling_averages
    Measurement.by_isp(isp)
               .where(timestamp: time_window)
               .select(:timestamp, :download, :upload, :ping)
               .order(timestamp: :asc)
               .to_a
               .each_cons(window_size)
               .map do |window|
      average_download = window.sum(&:download) / window_size
      average_upload = window.sum(&:upload) / window_size
      average_ping = window.sum(&:ping).to_f / window_size
      timestamp = window.map(&:timestamp).sort[window_size / 2]

      [timestamp, average_download, average_upload, average_ping]
    end
  end

  def start_date
    @start_date ||= Date.iso8601 params.require(:start_date)
  end

  def time_window
    start_date.beginning_of_day..end_date.end_of_day
  end

  def window_size
    @window_size ||= Integer params.require(:window_size)
  end
end
