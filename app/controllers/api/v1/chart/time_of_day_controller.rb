class Api::V1::Chart::TimeOfDayController < ApplicationController
  def index
    @result = time_of_day_averages
  end

  private

  def all_measurements
     Measurement.by_isp(isp).select(:timestamp, :download, :upload, :ping)
  end

  def group_by_10_minute_intervals
    @group_by_10_minute_intervals ||= all_measurements.group_by do |measurement|
      measurement.timestamp.hour * 60 + (measurement.timestamp.min / 10) * 10
    end
  end

  def isp
    @isp ||= params.require :isp
  end

  def time_of_day_averages
    group_by_10_minute_intervals.sort.map do |(key, measurements)|
      average_download = measurements.sum(&:download) / measurements.size
      average_upload = measurements.sum(&:upload)/ measurements.size
      average_ping = (measurements.sum(&:ping).to_f / measurements.size)
                     .round(2)

      [key, average_download, average_upload, average_ping]
    end
  end
end
