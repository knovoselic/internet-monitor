class Api::V1::Chart::RawDataController < ApplicationController
  def index
    @result = Measurement.by_isp(isp)
                         .where(timestamp: time_window)
                         .order(timestamp: :asc)
                         .pluck(:timestamp, :download, :upload, :ping)
  end

  private

  def isp
    params.require :isp
  end

  def time_window
    start_date.beginning_of_day..end_date.end_of_day
  end

  def start_date
    @start_date ||= Date.iso8601 params.require(:start_date)
  end

  def end_date
    @end_date ||= Date.iso8601 params.require(:end_date)
  end
end
