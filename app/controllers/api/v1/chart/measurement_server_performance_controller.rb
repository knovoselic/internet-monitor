class Api::V1::Chart::MeasurementServerPerformanceController < ApplicationController
  def index
    @result = measurements.sort
    @server_metadata = server_metadata
  end

  private

  def isp
    params.require :isp
  end

  def measurements
    measurements_by_server.map do |(server_id, measurements)|
      server_position = server_id_to_position[server_id]
      download_values_sorted = measurements.map(&:download).sort

      average_download = download_values_sorted.sum / measurements.size
      maximum_download = download_values_sorted.max

      [
        server_position,
        nth_percentile(5, download_values_sorted),
        average_download,
        maximum_download
      ]
    end
  end

  def measurements_by_server
    @measurements_by_server ||=
      Measurement.by_isp(isp)
                 .includes(:measurement_server)
                 .group_by { |item| item.measurement_server.server_id }
                 .select { |key, values| values.size > 10 }
  end

  def nth_percentile(k, sorted_data_set)
    index = (sorted_data_set.size * k / 100.0).round(0).to_i;
    sorted_data_set[index]
  end

  def server_metadata
    server_id_to_position.each_with_object({}) do |(server_id, index), memo|
      server = measurements_by_server[server_id].first.measurement_server

      memo[index] = {
        name: "#{server.sponsor} #{server.name}, #{server.country}",
        server_id: server_id,
        measurements: measurements_by_server[server_id].size
      }
    end
  end

  def server_id_to_position
    @server_id_to_position ||=
      measurements_by_server.keys
                            .sort
                            .each_with_index
                            .each_with_object({}) do |(server_id, index), memo|
      memo[server_id] = index
    end
  end
end
