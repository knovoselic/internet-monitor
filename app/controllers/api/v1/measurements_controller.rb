class Api::V1::MeasurementsController < ApiController
  PERMITTED_PARAMETERS = [
    :download,
    :upload,
    :ping,
    :timestamp,
    :bytes_sent,
    :bytes_received,
    :created_at,
    :updated_at,
    server: %i[url lat lon name country cc sponsor id host d latency],
    client: %i[ip lat lon isp isprating country]
  ].freeze

  before_action :initialize_create_params

  def create
    Measurement.create! @create_params

    head :no_content
  end

  private

  def initialize_create_params
    process_root_object
    process_server_object
    process_client_object
  end

  def process_client_object
    return unless @create_params.key? :client

    client_hash = @create_params.delete :client
    rename_key_if_present client_hash, :isprating, :isp_rating
    @create_params[:measurement_client_attributes] = client_hash
  end

  def process_root_object
    @create_params = params.permit(*PERMITTED_PARAMETERS)
    @create_params[:timestamp] ||= Time.current
  end

  def process_server_object
    return unless @create_params.key? :server

    server_hash = @create_params.delete :server
    server_hash.slice!(*PERMITTED_PARAMETERS.last.fetch(:server))
    rename_key_if_present server_hash, :id, :server_id
    @create_params[:measurement_server_attributes] = server_hash
  end

  def rename_key_if_present(hash, old_name, new_name)
    return unless hash.key? old_name
    hash[new_name] = hash.delete old_name
  end
end
