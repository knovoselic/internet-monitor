module ApplicationHelper
  def available_isps
    @available_isps ||= MeasurementClient.select(:isp).distinct.pluck(:isp)
  end

  def nav_link(text, url, options = {})
    link_css_classes = 'nav-link'
    link_css_classes += ' active' if current_page? url

    content_tag(:li, class: 'nav-item') do
      link_to text, url, class: link_css_classes
    end
  end
end
