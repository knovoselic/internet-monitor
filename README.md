# README

## Server
### Initial setup
Before you can start the server, you need to create a credentials file for your application. You can do this on production or development machine. If you decide to do it on development machine, you'll need to copy `config/credentials/production.key` and `config/credentials/production.yml.enc` files to the production server.

First you'll have to remove `config/credentials/production.yml.enc` file (if it already exists and you don't have the key for it). After that, run the following command:
```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml run web bin/rails credentials:edit --environment production
```

After the development image builds, credentials file will get opened in `vim` editor. You need to add two secret values to that file:
```yaml
api_authorization_key: something-long-and-random
secret_key_base: something-long-and-random
```

You can use the following command to generate secure, randomized values:
```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml run web bin/rails secret
```

Save the file and exit the editor. Copy the file to production machine (if it's been created somewhere else).
### Running the server
Make sure you have latest `docker` and `docker-compose` installed. If so, running the server is as simple as running the following command:
```bash
docker-compose up web
```
## Client
### Configuration
Before running the client you will probably need to update its configuration. The client configuration is defined in `docker-compose.yml` file, under `services.client.environment` key. To override these values, you can create `docker-compose.override.yml` files with the following contents:
```yaml
version: '3.7'
services:
  client:
    environment:
      - WEB_AUTHORIZATION_KEY=1234 # same value as api_authorization_key defined in server credentials file
      - WEB_HOST=http://web:3000 # URL to the server, without trailing slash
```
### Running the client
Once you have correct configuration in place, just run:
```
docker-compose up client
```