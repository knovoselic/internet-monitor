Rails.application.routes.draw do
  root to: redirect('/statistics/overall?isp=all')

  namespace :api do
    namespace :v1 do
      namespace :chart do
        resources :raw_data, only: %i[index]
        resources :rolling_averages, only: %i[index]
        resources :time_of_day, only: %i[index]
        resources :measurement_server_performance, only: %i[index]
      end
      resources :measurements, only: %i[create]
    end
  end

  namespace :statistics do
    get 'overall', to: 'overall#index'
    get 'measurement_servers', to: 'measurement_servers#index'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
