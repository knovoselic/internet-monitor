class CreateMeasurementClients < ActiveRecord::Migration[6.0]
  def change
    create_table :measurement_clients do |t|
      t.belongs_to :measurement, null: false, foreign_key: true

      t.string :ip
      t.decimal :lat, precision: 9, scale: 6
      t.decimal :lon, precision: 9, scale: 6
      t.string :isp
      t.decimal :isp_rating, precision: 4, scale: 2
      t.string :country

      t.timestamps
    end
  end
end
