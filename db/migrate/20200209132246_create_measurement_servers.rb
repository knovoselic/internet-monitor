class CreateMeasurementServers < ActiveRecord::Migration[6.0]
  def change
    create_table :measurement_servers do |t|
      t.belongs_to :measurement, null: false, foreign_key: true

      t.string :url
      t.decimal :lat, precision: 9, scale: 6
      t.decimal :lon, precision: 9, scale: 6
      t.string :name
      t.string :country
      t.string :cc
      t.string :sponsor
      t.integer :server_id
      t.string :host
      t.decimal :d, precision: 20, scale: 16
      t.integer :latency

      t.timestamps
    end
  end
end
