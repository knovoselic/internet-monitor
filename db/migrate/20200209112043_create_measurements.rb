class CreateMeasurements < ActiveRecord::Migration[6.0]
  def change
    create_table :measurements do |t|
      t.integer :download, null: false
      t.integer :upload, null: false
      t.integer :ping
      t.datetime :timestamp
      t.integer :bytes_sent
      t.integer :bytes_received

      t.timestamps
    end
  end
end
