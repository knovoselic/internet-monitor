# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_09_134545) do

  create_table "measurement_clients", force: :cascade do |t|
    t.integer "measurement_id", null: false
    t.string "ip"
    t.decimal "lat", precision: 9, scale: 6
    t.decimal "lon", precision: 9, scale: 6
    t.string "isp"
    t.decimal "isp_rating", precision: 4, scale: 2
    t.string "country"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["measurement_id"], name: "index_measurement_clients_on_measurement_id"
  end

  create_table "measurement_servers", force: :cascade do |t|
    t.integer "measurement_id", null: false
    t.string "url"
    t.decimal "lat", precision: 9, scale: 6
    t.decimal "lon", precision: 9, scale: 6
    t.string "name"
    t.string "country"
    t.string "cc"
    t.string "sponsor"
    t.integer "server_id"
    t.string "host"
    t.decimal "d", precision: 20, scale: 16
    t.integer "latency"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["measurement_id"], name: "index_measurement_servers_on_measurement_id"
  end

  create_table "measurements", force: :cascade do |t|
    t.integer "download", null: false
    t.integer "upload", null: false
    t.integer "ping"
    t.datetime "timestamp"
    t.integer "bytes_sent"
    t.integer "bytes_received"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "measurement_clients", "measurements"
  add_foreign_key "measurement_servers", "measurements"
end
